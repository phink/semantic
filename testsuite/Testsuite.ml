open Teddy
open MLR_Interface
open MLR_Types
open MLR_Run
open MLR_PPrint

let activate (module T : TRANS) ?(env : Gamma.t option = None) program
  : (module ACTIVE) =
  (module struct
    let translation =
      T.transform program
    let ty_env = match env with
      | None -> Gamma.to_list (typecheck ~ty_env:T.initial_tyenv translation)
      | Some env -> Gamma.to_list env
    let (memory,env) =
      evaluate_program ~env:T.initial_env translation
    let display () =
      print (print_infos memory env ty_env) translation
  end)

let trans_full program =
  reset ();
  let ty_env = typecheck program in
  let cps = activate (module MLR_CPS) ~env:(Some ty_env) program in
  let vnf = activate (module MLR_VNF) ~env:(Some ty_env) (translation cps) in
  let ccc = activate (module MLR_CCC) ~env:(Some ty_env) (translation vnf) in
  let hos = activate (module MLR_Hoisting) ~env:(Some ty_env) (translation ccc)
  in display hos ()

let trans_id program =
  reset ();
  let ty_env = typecheck program in
  let id = activate (module MLR_Identity) ~env:(Some ty_env) program
  in display id ()

module BASIC =
  Make (struct
    let language_extension = "mlref"
    let path = "/home/phink/repo/ml-ref/testsuite/test/"
    let action program = trans_id (parse_string program)
  end)

module TRANSFORMED =
  Make (struct
    let language_extension = "mlref"
    let path = "/home/phink/repo/ml-ref/testsuite/test/"
    let action program = trans_full (parse_string program)
  end)


let () =
  print_endline "******* TESTING NORMAL EVALUATION *******";
  BASIC.main ~verbose:false ();
  print_newline ();
  print_endline "******* TESTING FULL TRANSFORMED EVALUATION *******";
  TRANSFORMED.main ~verbose:false ()

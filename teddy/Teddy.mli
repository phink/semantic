module type Configuration =
  sig
    val path : string
    val language_extension : string
    val action : string -> string
  end

module Make :
functor (Configuration : Configuration) ->
sig
  val main : verbose:bool -> unit -> unit
end


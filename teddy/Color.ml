type color =
  { r : int
  ; g : int
  ; b : int
  }

let mk r g b =
  { r = r
  ; g = g
  ; b = b
  }

let ( <+> ) a b =
  { r = a.r lor b.r
  ; g = a.g lor b.g
  ; b = a.b lor b.b
  }

let black   = mk 0 0 0
let white   = mk 1 1 1

let red     = mk 1 0 0
let green   = mk 0 1 0
let blue    = mk 0 0 1

let yellow  = green <+> red
let cyan    = green <+> blue
let magenta = blue  <+> red

let ( ! ) {r;g;b} = r + g lsl 1 + b lsl 2

let color color s =
  Printf.sprintf "\027[38;5;%dm%s\027[0m" !color s

let light color s =
  Printf.sprintf "\027[38;5;%dm%s\027[0m" (!color + 8) s

module Print = struct
    
    let black = color black
    let white = color white
			 
    let red   = color red
    let green = color green
    let blue  = color blue
			 
    let orange  = color yellow
    let cyan    = color cyan
    let magenta = color magenta

    let yellow  = light yellow
			
  end



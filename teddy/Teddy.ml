module type Configuration =
sig
  val path : string
  val language_extension : string
  val action : string -> string
end

open Filename

module Make (Configuration : Configuration) = struct

  open Configuration

  let template kind =
    "." ^ kind ^ "." ^ language_extension

  let input_ex    = template "input"
  let expected_ex = template "expected"
  let output_ex   = template "output"

  let input    name = name ^ input_ex
  let expected name = name ^ expected_ex
  let output   name = name ^ output_ex		       

  let list () =
    let cd = Unix.opendir path in
    let rec aux acc = match Unix.readdir cd with
      | (exception End_of_file) -> acc
      | file -> aux (file :: acc)
    in aux []

  let end_with extension filename =
    let n = String.length extension in
    let m = String.length filename - n in
    let rec aux i =
      i = n || extension.[i] = filename.[m + i] && aux (succ i)
    in m > 0 && aux 0

  let is_input    = end_with input_ex
  let is_expected = end_with expected_ex
  let is_output   = end_with output_ex

  let name_from extension target =
    String.(sub target 0 (length target - length extension))
      
  let name_from_input    = name_from input_ex
  let name_from_expected = name_from expected_ex
  let name_from_output   = name_from output_ex

  let filter p =
    List.fold_left (fun acc x -> if p x then x :: acc else acc) [] (list ())

  let inputs    () = filter is_input
  let expecteds () = filter is_expected
  let outputs   () = filter is_output

  module M = Map.Make(String)

  let init_map () =
    List.fold_left
      (fun map s -> M.add (name_from_input s) false map)
      M.empty
      (inputs ())

  let exists_input key map = match M.find key map with
    | (exception Not_found) -> Missing.input key
    | value -> M.add key true map

  let exists_input_forall map =
    List.fold_left
      (fun map key -> exists_input (name_from_expected key) map)
      map
      (expecteds ())

  let exists_expected_forall map =
    M.iter (fun key data -> if data then () else Missing.expected key) map;
    map
    
  (* CLEANING *)
    
  let print_cleaning () =
    let targets = outputs () in
    let number = List.length targets in
    if number > 0 then
      Printf.printf "- Removing previous output files. (%d files)\n" number

  let clean ~verbose () =
    if verbose then print_cleaning ();
    outputs ()
    |> List.map (fun output -> path ^ output)
    |> List.iter Unix.unlink

  let keys map = List.map fst (M.bindings map)
  
  let init ~verbose () =
    clean verbose ();
    init_map ()
    |> exists_input_forall
    |> exists_expected_forall
    |> keys

  (* Read and Write *)

  let read name =
    let name = (path ^ input name) in
    let src = open_in name in
    let result = ref "" in
    let rec aux acc =
      try
        acc := !acc ^ String.make 1 (input_char src);
        aux acc
      with End_of_file -> !acc
    in
    let res = aux result
    in close_in src; res

  let write name data =
    let name = (path ^ output name) in
    let target = open_out name in
    output_string target data;
    close_out target

  (* Out_channel.write_all (path ^ output name) ~data *)
      
  (* ACTION *)
      
  let print_inputfiles = function
    | [] ->
      print_endline (Color.Print.orange "W: Cannot find any input file")
    | xs ->
      print_endline "- The following test files are concerned.";
      List.iter (fun n -> Printf.printf "-- %s\n" (Color.Print.yellow n)) xs

  let print_acting_start () =
    Printf.printf "- Start acting on files.\n"

  let print_acting_before name =
    Printf.printf "-- Acting on %s..\t" name

  let print_acting_after time =
    Printf.printf "%f\n" time

  let action ~verbose name =
    if verbose then print_acting_before name;
    let time_before = Unix.time () in
    let acted = action (read name) in
    let time_after = Unix.time () in
    let time = time_after -. time_before in
    if verbose then print_acting_after time;
    write name acted

  let actions ~verbose files =
    if verbose then
      begin
        print_inputfiles files;
        print_acting_start ()
      end;
    List.iter (action ~verbose) files

  (* EXPECTATION *)

  let print_expectation () =
    print_endline "- Now checking expectation."
      
  let print_expectation_before name =
    let colored_name = Color.Print.cyan name
    in Printf.printf "-- [%s]: Checking expectation..\t" colored_name

  let print_expectation_after result =
    let open Color.Print in
    let presult = if result then green "OK" else red "FAIL"
    in print_endline presult

  let print_result_expectation (t,s) =
    let color = Color.Print.(if s = t then green else red) in
    Printf.printf "[%s]  Total: %d \tSuccess: %d.\n" (color "RESULT") t s

  let print_generated_outputs () = match outputs () with
    | [] -> ()
    | files ->
      print_endline "- Output files were generated for the following tests.";
      List.iter (Printf.printf "-- %s\n") files

  (* Equality between files with a simple trick to avoid when a backline is
     generated by emacs at the end of the file. *)

  let input_char src = try Some (input_char src) with _ -> None
    
  let ( == ) expected output =
    let rec aux () = match (input_char expected, input_char output) with
      (* | Some ' ',  None -> aux () *)
      (* | Some '\n', None -> aux () *)
      | None, None -> true
      | Some x, Some y when x = y -> aux ()
      | _ -> false
    in
    let res = aux () in
    close_in expected; close_in output;
    res

  let expectation ~verbose name (t,s) =
    let open Unix in
    if verbose then print_expectation_before name;
    let output_path = path ^ output   name
    and expect_path = path ^ expected name in
    let fd_output = openfile output_path [O_RDONLY] 0o644 in
    let fd_expect = openfile expect_path [O_RDONLY] 0o644 in
    let eq = in_channel_of_descr fd_expect == in_channel_of_descr fd_output in
    if verbose then print_expectation_after eq;
    if eq then unlink output_path;
    (succ t, if eq then succ s else s)
    
  let expectations ~verbose files =
    if verbose then print_expectation ();
    let f result file = expectation ~verbose file result in
    let result = List.fold_left f (0,0) files in
    print_result_expectation result;
    if verbose then print_generated_outputs ()

  (* ENTRANCE *)

  let main ~verbose () =
    let files = init ~verbose () in
    actions      ~verbose files;
    expectations ~verbose files
end

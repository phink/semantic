(* Exception for missing files. *)

exception Missing_file of string	

let input name =
  raise @@ Missing_file ("Missing source file for " ^ name)

let expected name =
  raise @@ Missing_file ("Missing expected file for " ^ name)

let break ?(verbose=true) () =
  if verbose then print_endline "Press enter to continue..";
  let _ = input_char stdin in
  ()

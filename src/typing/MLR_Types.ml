open MLR_AST

(* 
   see http://okmij.org/ftp/ML/generalization.html

 *)

type tyname = string
type id     = int
type level  = int

type ty =
  | TyCon   of tyname
  | TyVar   of tyvar ref
  | TyArrow of ty * ty
  | TyRef   of ty

and tyvar =
  | Unbound of id * level
  | Link    of ty
  | Generic of id

let cmpt = ref 0
let lvl = ref 0
    
let reset () = cmpt := 0; lvl := 0

let generate () = incr cmpt; !cmpt

let new_var lvl () =
  TyVar (ref (Unbound (generate (), lvl)))

exception Type_error of string

let error err = raise (Type_error err)

module Gamma = struct
  module M = Map.Make (String)      
  type t = ty M.t
  let empty = M.empty
  let add = M.add
  let get = M.find
  let to_list = M.bindings
end

exception Unify of (ty * ty)

let occurs_check ty_id ty_lvl ty =
  let rec aux = function
    | TyCon _ -> ()
    | TyVar tv -> begin
        match !tv with
        | Link ty -> aux ty
        | Unbound (ty_id',ty_lvl') ->
          if ty_id = ty_id' then error "recursive types not allowed";
          if ty_lvl < ty_lvl' then tv := Unbound (ty_id', ty_lvl)
        | Generic _ -> assert false
      end
    | TyArrow (ty1,ty2) -> aux ty1; aux ty2
    | TyRef ty -> aux ty
  in aux ty

(* The unifier solve equation (t1 = t2) *)

let rec solve ty1 ty2 = match (ty1,ty2) with
  | (TyCon n1, TyCon n2) when n1 = n2 -> ()
  | ( TyVar {contents = Unbound (id1,_)}
    , TyVar {contents = Unbound (id2,_)}) when id1 = id2 ->
    assert false
  | (TyVar v, ty) | (ty, TyVar v) -> begin
      match !v with
      | Link ty' ->
        solve ty ty'
      | Unbound (ty_id,lvl) ->
        occurs_check ty_id lvl ty;
        v := Link ty
      | _ -> error "unify tyvar"
    end
  | (TyArrow (t1,o1), TyArrow (t2,o2)) ->
    solve t1 t2; solve o1 o2
  | (TyRef t1, TyRef t2) ->
    solve t1 t2
  | _ -> raise (Unify (ty1,ty2))

let rec generalize lvl ty = match ty with
  | TyVar v -> begin match !v with
      | Unbound (id,lvl') when lvl < lvl' ->
        TyVar (ref (Generic id))
      | Link ty -> generalize lvl ty
      | _ -> ty
    end
  | TyCon _ -> ty
  | TyArrow (ty1,ty2) ->
    TyArrow (generalize lvl ty1, generalize lvl ty2)    
  | TyRef _ -> ty

let instantiate lvl ty =
  let vars = Hashtbl.create 10 in
  let rec aux ty = match ty with
    | TyCon _ -> ty
    | TyVar v as ty -> begin match !v with
        | Link ty -> aux ty
        | Generic id -> begin
            try Hashtbl.find vars id
            with Not_found ->
              let fresh_var = new_var lvl () in
              Hashtbl.add vars id fresh_var;
              fresh_var
          end
        | Unbound _ -> ty
      end
    | TyArrow (ty1,ty2) -> 
      TyArrow (aux ty1, aux ty2)
    | TyRef ty -> TyRef (aux ty)
  in aux ty

(* Value restriction on polymorphism
   ─────────────────────────────────

   On ne généralise que les expressions qui sont non-expansives,
   c'est à dire font la forme même garantit que leur évaluation 
   ne crée pas de références.

   Γ ⊢ t₁ : τ₁     
   σ = Gen (τ₁,Γ) si a₁ non expansive | τ₁ sinon
   Γ , x ∙ σ ⊢ t₂ ⊢ τ
   ─────────────────────────────────────────────────────────
                  Γ ⊢ let x = t₁ in t₂ : τ

   On exclut des expressions non-expansives les applications de 
   l'opérateur ref (qui crée une nouvelle référence), ainsi que les 
   applications de fonctions (car en géenéral on ne sait pas si le 
   corps de la fonction va créer ou non de nouvelles références).
   
   reference: http://pauillac.inria.fr/~xleroy/dea/typage/imperatif.pdf
 *)

let rec is_not_expansive = function
  | Var _ | Nat _ | Bool _ | Unit -> true
  | Plus (t1,t2) | Minus (t1,t2) ->
    is_not_expansive t1 && is_not_expansive t2
  | Lt (t1,t2)  | And (t1,t2) ->
    is_not_expansive t1 && is_not_expansive t2
  | Lam (_,t) | Not t ->
    is_not_expansive t
  | IfThenElse (b,t1,t2) ->
    is_not_expansive b  &&
    is_not_expansive t1 && is_not_expansive t2
  | LetIn (_,t1,t2) ->
    is_not_expansive t1 && is_not_expansive t2
  | Attr (_,_) | Address _ | App (_,_) | Ref _ | Bang _ -> false

let rec from_fun = function
  | TyArrow (ty1,ty2) -> (ty1,ty2)
  | TyVar v -> begin match !v with
      | Link ty -> from_fun ty
      | Unbound (id,lvl) ->
        let nv = new_var lvl () in
        let ty = new_var lvl () in
        v := Link (TyArrow (nv,ty));
        (nv,ty)
      |  _ -> error "expected type arrow"
    end
  |  _ -> error "expected type arrow"

let ( @-> ) t1 t2 = TyArrow (t1, t2)

let rec infer env lvl = function
  | Var id -> begin
      try instantiate lvl (Gamma.get id env)
      with Not_found -> error ("variable not found " ^ id)
    end
    
  | Lam (x,t) ->
    let alpha = new_var lvl () in
    let fn_env = Gamma.add x alpha env in
    let ty = infer fn_env lvl t in
    alpha @-> ty

  | App (f,x) ->
    let (ty1,ty2) = from_fun (infer env lvl f) in
    let ty_x = infer env lvl x in
    solve ty1 ty_x;
    ty2

  (* Constant *)
  | Nat _ -> TyCon "Nat"
  | Bool _ -> TyCon "Bool"
  | Unit -> TyCon "Unit"

  (* Defined symbols *)
  | Plus (a,b) ->
    let ty_a = infer env lvl a in
    let ty_b = infer env lvl b in
    solve ty_a (TyCon "Nat");
    solve ty_b (TyCon "Nat");
    TyCon "Nat"

  | Minus (a,b) ->
    let ty_a = infer env lvl a in
    let ty_b = infer env lvl b in
    solve ty_a (TyCon "Nat");
    solve ty_b (TyCon "Nat");
    TyCon "Nat"

  | Lt (a,b) ->
    let ty_a = infer env lvl a in
    let ty_b = infer env lvl b in
    solve ty_a (TyCon "Nat");
    solve ty_b (TyCon "Nat");
    TyCon "Bool"

  | And (a,b) ->
    let ty_a = infer env lvl a in
    let ty_b = infer env lvl b in
    solve ty_a (TyCon "Bool");
    solve ty_b (TyCon "Bool");
    TyCon "Bool"

  | Not a ->
    let ty_a = infer env lvl a in
    solve ty_a (TyCon "Bool");
    TyCon "Bool"

  | Ref a ->
    let ty_a = infer env lvl a in
    TyRef ty_a

  | Address _ ->
    (* Address should not be manipulated *)
    assert false

  | Bang a ->
    let ty_a = infer env lvl a in
    let new_ty = new_var lvl () in
    solve (TyRef new_ty) (ty_a);
    new_ty
    
  | Attr (t1,t2) ->
    let ty_1 = infer env lvl t1 in
    let ty_2 = infer env lvl t2 in
    solve ty_1 (TyRef ty_2);
    TyCon ("Unit")
    
  | LetIn (x,t1,t2) ->
    let ty_raw_x = infer env (succ lvl) t1 in
    let ty_x =
      if is_not_expansive t1 then generalize lvl ty_raw_x
      else ty_raw_x
    in
    let new_env = Gamma.add x ty_x env in
    infer new_env lvl t2
                    
  | IfThenElse (b,t1,t2) ->
    let ty_b  = infer env lvl b in
    let ty_t1 = infer env lvl t1 in
    let ty_t2 = infer env lvl t2 in
    solve ty_b (TyCon "Bool");
    solve ty_t1 ty_t2;
    ty_t1

let typecheck ?(ty_env=Gamma.empty) pgm =
  List.fold_left (
    fun env (Let vb) ->
      let ty = generalize (-1) @@ infer env !lvl vb.vb_exp
      in incr lvl; Gamma.add vb.vb_id ty env
  ) ty_env pgm

open MLR_AST

let name = "CLOSURE CONVERSION"

let fresh_var =
  let i = ref 0 in
  fun () -> incr i; "CCC_" ^ string_of_int !i

(* Pairing using Church Encoding *)

let pairing xs = match xs with
  | [] -> Unit
  | xs -> Lam ("P", fold_app "P" (List.rev xs))

let proj i xs =
  let xi = List.nth xs i
  in Lam ("P", Var "P" $ unfold_lam (Var xi) xs)
    
(* Identifiers *)

let c_id = "C"
let e_id = "E"

let generate_letin_pi xs rhs t =
  let rec aux n = function
    | [] -> t
    | z::zs ->
      LetIn (z, proj n xs $ rhs, aux (succ n) zs)
  in aux 0 xs
    
(* Transformation *)

let is_value = function
  | Lam _ | Nat _ | Bool _ | Address _ | Unit -> true
  | _ -> false

let rec most_left_app f v = function
  | App (x,y) -> App (most_left_app f v x,y)
  | m -> v := Some m; f m
           
let rec transf m = match m with

  | App (x,y) ->
    let r = ref None in
    let f _ = Var "C" $ Var "E" in
    let new_app = most_left_app f r x $ y in
    begin match !r with
      | Some v -> generate_letin_pi ["C";"E"] v new_app
      | None -> assert false
    end    
  | LetIn (id, Lam (x,t),m) ->
    let ccc_m = transf m in
    let zs = remove x (ftv t) in
    let let_zs n = generate_letin_pi zs (Var "E") (transf n)  in
    let let_x = LetIn (id, pairing ["C";"E"], ccc_m) in
    let let_e = LetIn ("E", pairing zs, let_x) in
    let let_c = LetIn ("C", Lam ("E", Lam (x, collapse_lam let_zs t)), let_e)
    in let_c
  | LetIn (x,m,n) -> LetIn (x,m, transf n)
  | m  -> failwith "CCC.TRANSF"

let new_halt_exp =
  let r = pairing ["C";"E"] in
  let let_e = LetIn ("E", Unit, r) in
  LetIn ("C", Lam ("E", Lam ("Hx", Var "Hx")), let_e)

let new_halt = Let {vb_id = "HALT"; vb_exp = new_halt_exp}

let initial_env =
  snd (MLR_Run.evaluate_program [new_halt])

let initial_tyenv = MLR_CPS.initial_tyenv

let transform ms =
  List.map (
    fun (Let {vb_id;vb_exp}) ->
      let new_exp = transf vb_exp in
      Let {vb_id; vb_exp = new_exp}
  ) ms

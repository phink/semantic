open MLR_AST

let name = "Hoisting"

let fresh_var =
  let i = ref 0 in
  fun () -> incr i; "H_" ^ string_of_int !i

let is_restricted = function
  | Var _ -> true 
  | App (p,_) ->
    let rec aux = function
      | Var "P"  -> true
      | App (p,_) -> aux p
      | _ -> false
    in aux p
  | _ -> false

let rec drop_in_lam =
  let rec aux acc = function
    | Lam (w,t) -> aux (w::acc) t
    | m -> (List.rev acc, m)
  in aux []

let rec trans im = match im with
  | LetIn (x,c, LetIn (y, Lam (z,t), m))
    when is_restricted c && not (List.mem x (ftv (Lam (z,t)))) ->
    trans @@ LetIn (y,Lam (z,t), LetIn (x,c, m))
  | LetIn (x, Lam (w,t),n) ->
    let (ws,rhs) = drop_in_lam (Lam (w,t)) in
    begin match rhs with
      | LetIn (y, Lam (z,t),m) ->
        let fvs = ftv (Lam (z,t)) in
        if (List.for_all (fun x -> not (List.mem x fvs)) ws) then
          trans @@ LetIn (y, Lam (z,t), LetIn (x, unfold_lam m ws,n))
        else failwith "deeep"
      | m -> im
    end
  | im -> im

let transform ms =
  List.map (
    fun (Let {vb_id;vb_exp}) ->
      let new_exp = trans vb_exp in
      Let {vb_id; vb_exp = new_exp}
  ) ms

let initial_env   = MLR_CCC.initial_env
let initial_tyenv = MLR_CCC.initial_tyenv

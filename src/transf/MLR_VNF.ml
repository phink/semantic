open MLR_AST

let name = "VALUE NAMED FORM"

(* VALUE NAMED FORM
   ────────────────

   This transformation introduces the Value named form [0].

   All the values will have a name.

   V ::= Lam | Nat | Bool | Address | Unit

   [0]: "Certifying and reasoning about cost annotations of functional programs."
         ─ Amadio and Regis-Gianas
*)

let is_value = function
  | Lam _ | Nat _ | Bool _ | Address _ | Unit -> true
  | _ -> false

let fresh_var =
  let i = ref 0 in
  fun () -> incr i; "VNF_" ^ string_of_int !i

(* VNF functions *)

let rec vnf_app m vs = match m with
  | m when is_value m ->
    let y = fresh_var () in 
    vs := (y,m) :: !vs; Var y
  | App (a,b) -> vnf_app a vs $ vnf_app b vs
  | Plus (a,b) -> vnf_app a vs +++ vnf_app b vs
  | Minus (a,b) -> vnf_app a vs --- vnf_app b vs
  | Lt (a,b) -> lt (vnf_app a vs) (vnf_app b vs)
  | And (a,b) -> vnf_app a vs & vnf_app b vs
  | Not a -> !- (vnf_app a vs)
  | Ref a -> mk_ref (vnf_app a vs)
  | Bang a -> bang (vnf_app a vs)
  | Attr (a,b) -> vnf_app a vs =:= vnf_app b vs
  | m -> (* m is a value *)
    vnf_raw m

and vnf_raw m = match m with
  
  | App _ ->
    let vs = ref [] in 
    let new_app = vnf_app m vs in
    List.fold_right (fun (y,v) acc -> vnf_value v acc y) !vs new_app

  | Plus (a,b) -> op2 a b (+++)

  | Var x -> Var x

  | Lam (x,m) ->
    let y = fresh_var () in
    LetIn(y, Lam (x, collapse_lam vnf_raw m), Var y)

  | Minus (m,n) -> op2 m n (---)

  | Lt (m,n) -> op2 m n lt

  | And (m,n) -> op2 m n (&)

  | Not m -> op1 m (!-)

  | Ref m -> op1 m mk_ref

  | Bang m -> op1 m bang

  | Attr (a,b) -> op2 a b (=:=)

  | LetIn (x,m,n) ->
    if is_value m then
      let y = fresh_var () in
      vnf_value m (LetIn (x, Var y, vnf_raw n)) y
    else LetIn (x, m, vnf_raw n)

  | IfThenElse (e1,e2,e3) -> op3 e1 e2 e3 if_then_else

  | value ->
    if is_value m then
      let y = fresh_var () in
      vnf_value value (Var y) y
    else failwith "not_a_value"
        
and op1 m f =
  if is_value m then
    let y = fresh_var () in
    vnf_value m (vnf_raw (f (Var y))) y
  else f (vnf_raw m)

and op2 m n f =
  if is_value m then
    let y = fresh_var () in
    vnf_value m (vnf_raw (f (Var y) n)) y
  else if is_value n then
    let y = fresh_var () in
    vnf_value n (vnf_raw (f m (Var y))) y
  else f (vnf_raw m) (vnf_raw n)

and op3 a b c f =
  if is_value a then
    let y = fresh_var () in
    vnf_value a (vnf_raw (f (Var y) b c)) y
  else
  if is_value b then
    let y = fresh_var () in
    vnf_value b (vnf_raw (f a (Var y) c)) y
  else
  if is_value c then
    let y = fresh_var () in
    vnf_value c (vnf_raw (f a b (Var y))) y
  else f (vnf_raw a) (vnf_raw b) (vnf_raw c)


and vnf_value v hole y = match v with
  | Lam (x,m) ->
    LetIn (y, Lam (x, collapse_lam vnf_raw m), hole)
  | Nat _ | Bool _ | Unit | Address _ ->
    LetIn (y, v, hole)
  | _ -> assert false
    
let transform ms =
  List.map (
    fun (Let {vb_id;vb_exp}) ->
      let new_exp = vnf_raw vb_exp
      in Let {vb_id; vb_exp = new_exp}
  ) ms

let initial_tyenv = MLR_CPS.initial_tyenv
let initial_env = MLR_CPS.initial_env

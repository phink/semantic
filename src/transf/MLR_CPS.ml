open MLR_AST

let name = "CONTINUATION PASSING STYLE"

(* CONTINUATION PASSING STYLE
   ──────────────────────────

   According to [1] we are going to use an optimized version of CPS
   transformation.

   ---------------------------
   | Original Transformation |
   ---------------------------

   x     =  λk. kx
   ─
   λx.M  =  λk. k(λx. M)
   ────               ─
   M N   =  λk. M(λm. N(λn. m n k)
   ───          ─     ─


   The derivations rules mentioned above for the original CPS transformation
   are from Plotkin [0]. Thes ones leads to useless redexes
   (called administrative redexes).

   Example:

     x y    = λk. (λk. k x) (λm. (λk. k y) (λn. m n k))
     ───    = λk. (λm. (λk. k y) (λn. m n k)) x
            = λk. (λk. k y) (λn. x n k))
            = λk. (λn. x n k)) y
            = λk. x y k

   There is a first variant of CPS that avoid some administrative redexes.
   Instead of λ-abstracting over the continuation variable k we turn k into
   an additional parameter of the computation that defines the transformation.

   [M ▷ k] : M is the source term and k a continuation term.
    ─
   x     ▷ k  =  k x
   ─
   λx. M ▷ k  =  k (λx. λk. M ▷ k)
   ─────                    ─
   M N   ▷ k  =  M ▷ λm. N ▷ λn. m n k
   ───           ─       ─

   So we now have:

   x y ▷ k = (λm. (λn. m n k) y) x
   ───

   which contains only 2 administrative redexes.

   In [1], Dargaye and Leroy uses Danvy and Nielsen's optimized version
   [2] that avoid generating any administrative redexed. This is the one
   we are going to use in this work.
   It's called "first-order one-pass CPS transformation".

   It distinguishes terms that are
   - atoms     A,B ∷= x | λx.m
   - not atoms P,Q ∷= M N.

   Hence the transformation uses two mutually recursives functions Ψ for
   atoms and ⟦M⟧ ➤ k for arbitrary terms M.

   ψ(x)    = x
   Ψ(λx.M) = λx. λk. ⟦M⟧ ➤ k

   ⟦A⟧   ➤ k  =  k Ψ(A)
   ⟦A B⟧ ➤ k  =  Ψ(A) ψ (B) k
   ⟦A Q⟧ ➤ k  =  ⟦Q⟧ ➤ λq. ψ(A) q k
   ⟦P B⟧ ➤ k  =  ⟦P⟧ ➤ λp. p ψ(B) k
   ⟦P Q⟧ ➤ k  =  ⟦P⟧ ➤ λp. ⟦Q⟧ ➤ λq. p q k

   And finally, we now have ⟦x y⟧ ➤ k = x y k.
   This example is illustated in file playground/optCPS.mlref

   [0]: "Call-by-name, call-by-value and the lambda-calculus."
         ─ Plotkin

   [1]: "Mechanized Verification of CPS Transformations."
        ─ Dargaye and Leroy

   [2]: "A First-Order One-Pass CPS Transformation.",
        "CPS transformation of beta-redexes."
        ─ Danvy and Nielsen
*)

(* Some generators. *)

let halt_id = "HALT"

let fresh_var =
  let i = ref 0 in
  fun () ->
    incr i;
    "X_" ^ string_of_int !i

exception NotAtomic of expr

let is_atom = function
  | Var _ | Lam _ | Nat _ | Bool _ | Address _ | Unit -> true
  | _ -> false

(* Initialisation with the (K) halt (i.e identity) continuation *)

(* [init t] = t ➣ HALT  *)

let rec init e = trans e (Var "HALT")

and psi m = match m with

  (* Ψ(x) = x *)
  | Var _ | Nat _ | Bool _ | Address _ | Unit  -> m

  (* Ψ(λx.M) = λx. λk. ⟦M⟧ ➤ k *)
  | Lam (x,m) ->
    let k = fresh_var () in
    Lam (x, Lam (k, trans m (Var k)))

  (* We cannot apply Ψ to not atomic expression. *)
  | otherwise -> raise (NotAtomic m)

and trans m k = match m with

  (* ⟦A⟧ ➤ k    =  k @ Ψ(A) *)
  | m when is_atom m -> k $ psi m

  (* ⟦A B⟧ ➤ k  =  Ψ(A) ψ(B) k *)
  | App (a,b) when is_atom a && is_atom b ->
    psi a $ psi b $ k

  (* ⟦A Q⟧ ➤ k  =  ⟦Q⟧ ➤ λq. ψ(A) q k *)
  | App (a,q) when is_atom a ->
    let x = fresh_var () in
    trans q (Lam (x, psi a $ Var x $ k))

  (* ⟦P B⟧ ➤ k  =  ⟦P⟧ ➤ λp. p ψ(B) k *)
  | App (p,b) when is_atom b ->
    let xp = fresh_var () in
    trans p (Lam (xp, Var xp $ psi b $ k))

  (* ⟦P Q⟧ ➤ k  =  ⟦P⟧ ➤ λp. ⟦Q⟧ ➤ λq. p q k *)
  | App (p,q) ->
    let xp = fresh_var () in
    let xq = fresh_var () in
    trans p (Lam (xp, trans q (Lam (xq, Var xp $ Var xq $ k))))

  | Plus (a,b) -> op2 a b k (+++)

  | Minus (a,b) -> op2 a b k (---)

  | Lt (a,b) -> op2 a b k lt

  | And (a,b) -> op2 a b k (&)

  | Not a -> op1 a k (!-)

  | Ref a -> op1 a k mk_ref

  | Bang a -> op1 a k bang

  | Attr (a,b) -> op2 a b k (=:=)

  | LetIn (x,m,n) ->
    let x0 = fresh_var () in
    trans m (Lam (x0, LetIn (x, Var x0, trans n k)))

  | IfThenElse (e1,e2,e3) ->
    op3 e1 e2 e3 k if_then_else

  | _ -> assert false

and op1 a k f =
  if is_atom a then
    k $ f (psi a)
  else
    let xa = fresh_var () in
    trans a (Lam (xa, k $ f (Var xa)))

and op2 a b k f =
  if is_atom a then
    if is_atom b then
      k $ f (psi a) (psi b)
    else
      let xb = fresh_var () in
      trans b (Lam (xb, k $ f (psi a) (Var xb)))
  else if is_atom b then
    let xa = fresh_var () in
    trans a (Lam (xa, k $ f (Var xa) (psi b)))
  else
    let xa = fresh_var () in
    let xb = fresh_var () in
    trans a (Lam (xa, trans b (Lam (xb, k $ f (Var xa) (Var xb)))))

and op3 a b c k f =
  let xa = fresh_var () in
  let xb = fresh_var () in
  let xc = fresh_var () in
  if is_atom a then
    if is_atom b then
      if is_atom c then
        k $ f (psi a) (psi b) (psi c)
      else
        trans c (Lam (xc, k $ f (psi a) (psi b) (Var xc)))
    else
      trans b (Lam (xb, trans c (Lam (xc, k $ f (psi a) (Var xb) (Var xc)))))
  else
    (* a is not an atom *)
  if is_atom b then
    if is_atom c then
      trans a (Lam (xa, k $ f (Var xa) (psi b) (psi c)))
    else
      trans a (Lam (xa, trans c (Lam (xc, k $ f (Var xa) (psi b) (Var xc)))))
  else
  if is_atom c then
    trans a (Lam (xa, trans b (Lam (xb, k $ f (Var xa) (Var xb) (psi c)))))
  else
    let tail =
      trans b (Lam (xb,trans c (Lam (xc, k $ f (Var xa) (Var xb) (Var xc)))))
    in
    trans a (Lam (xa, tail))

let transform ms =
  List.map (fun (Let {vb_id;vb_exp}) ->
      let new_exp = init vb_exp in
      Let {vb_id; vb_exp = new_exp}) ms

let initial_tyenv =
  let open MLR_Types in
  let open Gamma in
  add "HALT" (new_var 9999 () @-> new_var 9999 ()) empty

let initial_env =
  let open MLR_Heap in
  update "HALT" MLR_Run.(VLam ("Hx", Closure (Var "Hx", empty))) empty

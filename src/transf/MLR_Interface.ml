(** All the transformations work on a subset of the original language. *)

type language = MLR_AST.t

(** Composition of transformations.

    We can create a composition function
    such that [M1 -| M2] returns [M2.transform ∘ M1.transform].
*)

module type TRANS = sig
  val name : string
  val transform : language -> language
  val initial_env : MLR_Run.env
  val initial_tyenv : MLR_Types.Gamma.t
end

let ( -| ) (module M1 : TRANS) (module M2 : TRANS) =
  fun program -> M2.transform (M1.transform program)

module type ACTIVE = sig
  val translation : language
  val display : unit -> string
end

let display (module A : ACTIVE) () = A.display ()
    
let translation (module A : ACTIVE) = A.translation

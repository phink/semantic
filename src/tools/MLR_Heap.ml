(** Function from keys to possibly values *)

type ('a,'b) t = 'a -> 'b option
    
let empty _ = None
  
let update r v h r' =
  if r = r' then Some v
  else h r'
      
let get r h = h r

let is_free r h = (h r = None)

let extract ?(err = fun () -> assert false) r h =
  match get r h with
  | None -> err ()
  | Some v -> v


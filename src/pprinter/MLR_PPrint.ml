open PPrint
open PPrintCombinators
open MLR_Run
open MLR_AST
open MLR_Types

let int n = string (string_of_int n)

module H = MLR_Heap

let ( ++ ) x y = x ^^ space ^^ y

let nat =
  let rec aux acc = function
    | Zero -> int acc
    | Succ n -> aux (succ acc) n
  in aux 0
    
let bool = function
  | True  -> string "true"
  | False -> string "false"

(* Print expressions *)

let rec expr = function
  | Var x -> string x
  | Lam (x,t) ->
    string "fun" ++ string x ++ string "->" ++ expr t
  | Nat n -> nat n
  | Bool b -> bool b
  | Address n -> string "ref[" ^^ int n ^^ string "]"
  | Unit -> string "()"
  | App (e1,e2) -> parens_expr e1 ++ parens_expr e2
  | Plus (n,m) -> parens_expr n ++ plus ++ parens_expr m
  | Minus (n,m) -> parens_expr n ++ minus ++ parens_expr m
  | Lt (n,m) -> parens_expr n ++ string "<" ++ parens_expr m
  | And (a,b) -> parens_expr a ++ string "and" ++ parens_expr b
  | Not e -> string "not" ++ parens_expr e
  | Ref e -> string "ref" ++ parens_expr e
  | Bang e -> string "!" ^^ parens_expr e
  | Attr (a,b) -> parens_expr a ++ string ":=" ++ parens_expr b
  | LetIn (x,m,n) ->
    group (string "let" ++ string x ++ equals ++ group (expr m)) ++ string "in"
           ^^ hardline ^^ expr n                
  | IfThenElse (b,t,f) ->
    string "if" ++ expr b ++ string "then" ++ expr t ++ string "else" ++ expr f

and parens_expr m = match m with
  | Var _ | Nat _ | Bool _ | Address _ | Unit -> expr m
  | _ -> parens (expr m)

let print_lets pgm =
  let definition (Let {vb_id;vb_exp}) =
    group (string "let" ++ string vb_id ++
           equals ^^ hardline ^^ expr vb_exp ^^ hardline)
  in separate_map hardline definition pgm ^^ hardline

(* Print types *)

let alphabet =
  [|"a";"b";"c";"d";"e";"f";"g";"h";"i";
    "j";"k";"l";"m";"n";"o";"p";"q";"r";
    "s";"t";"u";"v";"w";"x";"y";"z"
  |]

let rec ty = function
  | TyCon t -> string t
  | TyArrow (ty1,ty2)->
    ty_parens ty1 ++ string "->" ++ ty ty2
  | TyVar v -> begin match !v with
      | Generic id ->
        let n = id mod 25 in
        string (Printf.sprintf "%s%d" alphabet.(n) n)
      | Unbound (id,_) ->
        let n = id mod 25 in
        string (Printf.sprintf "_%s%d" alphabet.(n) n)
      | Link t -> ty t
    end
  | TyRef t -> string "Ref" ++ ty_parens t

and ty_parens t = match t with
  | (TyArrow _) | TyRef _ -> parens (ty t)
  | TyVar {contents = Link t} -> ty_parens t
  | _ -> ty t
    
(* Print values *)
                   
let rec value mem = function
  | VUnit -> string "()"
  | VLam (x,t) -> string "<fun>"
  | VNat n -> nat n
  | VBool b -> bool b
  | VAddress n -> string "ref[" ^^ value mem (H.extract n mem) ^^ string "]"

let vb venv mem ctx (Let {vb_id}) =
  match H.get vb_id venv with
  | Some v ->
    string "val" ++ string vb_id ++ colon ++
    (try ty (List.assoc vb_id ctx) with _ -> string "none")
    ++ equals ++ value mem v
    ^^ hardline
  | None -> failwith "pprint vb"
              
let to_string f x =
  let buffer = Buffer.create 13 in
  PPrintEngine.ToBuffer.pretty 0.8 80 buffer (f x);
  Buffer.contents buffer

let parse lexer_init input =
  lexer_init input
  |> MLR_Parser.program MLR_Lexer.token

let parse_string =
  parse Lexing.from_string
    
let print f ast =
  to_string f ast

let rec print_infos venv mem ctx pgm =
  concat_map (print_info mem venv ctx) pgm

and print_info venv mem ctx x = vb venv mem ctx x

(* Printing exception *)

let print_exn =
  let open Printf in function
    | Unify (t1,t2) ->
      sprintf "Cannot unify type %s and type %s" (print ty t1) (print ty t2)
    | ExpectingFun v ->
      sprintf "Expecting function but find value \"%s\"" (print (value H.empty) v)
    | _ -> failwith "print_exn"

let () = Printexc.register_printer (
    fun exn ->
      try Some (print_exn exn)
      with _ -> None)


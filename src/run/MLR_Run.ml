open MLR_AST
open MLR_Lib

module H = MLR_Heap

(** Values *)

(* 𝑉 ∷= λ𝑥. M | 𝑛 | 𝑏 | 𝑝 | () *)

type value =
  | VUnit
  | VLam of id * closure
  | VNat of nat
  | VBool of boolean
  | VAddress of int

and closure = Closure of expr * env

(** Environment *)

and env = (id, value) H.t

(** The Heap "configuration" for memory abstraction *)

type memory = (reference, value) H.t

exception OverridingRef

let generate : unit -> reference =
  let i = ref 0
  in fun () -> incr i; !i

let alloc heap value =
  let gen_ref = generate () in
  if not (H.is_free gen_ref heap) then raise OverridingRef;
  let new_heap = H.update gen_ref value heap
  in (new_heap,gen_ref)

(* Result type now available by default in OCAML 4.03 :) *)

type ('a,'b) result = Ok of 'a | Error of 'b

let return x = Ok x

let fail err = Error err

(** Contexts *)

type context =
  (* [] *)
  | EHole
  (* @(E,M) *)
  | EApp_l of context * expr * env
  (* @(V,E) *)
  | EApp_r of value * context * env
  (* E + M *)
  | EPlus_l of context * expr * env
  (* V + E *)
  | EPlus_r of value * context * env
  (* E + M *)
  | EMinus_l of context * expr * env
  (* V + E *)
  | EMinus_r of value * context * env
  (* E < M *)
  | ELt_l of context * expr * env
  (* V < E *)
  | ELt_r of value * context * env
  (* E ∧ M *)
  | EAnd_l of context * expr * env
  (* V ∧ E *)
  | EAnd_r of value * context * env
  (* ¬ E *)
  | ENot of context
  (* ref E *)
  | ERef of context
  (* !E *)
  | EBang of context
  (* E ≔ M *)
  | EAttr_l of context * expr * env
  (* V ≔ E *)
  | EAttr_r of value * context * env
  (* let x = E in M *)
  | ELetIn of string * context * expr * env
  (* if E then M else M *)
  | EIfThenElse of context * expr * expr * env

(** A small step abstract machine

    Is defined as a state-transition function together with a 'driver loop',
    i.e a function implementing the iteration of this transition function
    towards a final state, if any.

    The machine has two states:
     - a final one (a halting state)
     - an intermadiate one (a configuration)
*)

type configuration =
  | Eval  of expr * env * memory * context
  | Apply of value * memory * context

type state =
  | Final of halting_state
  | Inter of configuration

and halting_state = (memory * value, exn) result

exception UnboundVariable of string

exception AddressOutOfBounds

(* Note: the following failwiths should not arise during evaluation.
   Otherwise, they are the result of a bad typechecker.

   ps: That's why we do not raise specific exceptions.

                 “Well-typed programs can’t go wrong“ !
*)

(** A state-transition function [move] maps a state to an intermediate state. *)

exception ExpectingFun of value

let move : configuration -> state = function

  (* Evaluation part *)

  | Eval (Var x,env,h,ctx) ->
    begin match H.get x env with
      | Some v -> Inter (Apply (v,h,ctx))
      | None -> Final (fail (UnboundVariable x))
    end
    
  | Eval (Lam (x,t),env,h,ctx) ->
    Inter (Apply (VLam (x,Closure (t,env)),h,ctx))
      
  | Eval (Nat n,env,h,ctx) ->
    Inter (Apply (VNat n,h,ctx))
      
  | Eval (Bool b,env,h,ctx) ->
    Inter (Apply (VBool b,h,ctx))
      
  | Eval (Address p,env,h,ctx) ->
    begin match H.get p h with
      | Some v -> Inter (Apply (v,h,ctx))
      | None -> Final (fail AddressOutOfBounds)
    end
    
  | Eval (Unit,env,h,ctx) ->
    Inter (Apply (VUnit,h,ctx))
      
  | Eval (App (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,EApp_l (ctx,m2,env)))
      
  | Eval (Plus (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,EPlus_l (ctx,m2,env)))
      
  | Eval (Minus (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,EMinus_l (ctx,m2,env)))

  | Eval (Lt (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,ELt_l (ctx,m2,env)))

  | Eval (And (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,EAnd_l (ctx,m2,env)))

  | Eval (Not m,env,h,ctx) ->
    Inter (Eval (m,env,h,ENot ctx))

  | Eval (Ref m,env,h,ctx) ->
    Inter (Eval (m,env,h,ERef ctx))

  | Eval (Bang m,env,h,ctx) ->
    Inter (Eval (m,env,h,EBang ctx))

  | Eval (Attr (m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,EAttr_l (ctx,m2,env)))

  | Eval (LetIn (x,m1,m2),env,h,ctx) ->
    Inter (Eval (m1,env,h,ELetIn (x,ctx,m2,env)))

  | Eval (IfThenElse (b,m1,m2),env,h,ctx) ->
    Inter (Eval (b,env,h,EIfThenElse (ctx,m1,m2,env)))

  (* Application part *)

  | Apply (v,h,EHole) ->
    Final (return (h,v))

  | Apply (v,h,EApp_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,EApp_r (v,ctx,env)))

  | Apply (v,h,EApp_r (f,ctx,env)) ->
    begin match f with
      | VLam (x,Closure (t,env)) ->
        Inter (Eval (t,H.update x v env,h,ctx))
      | _ -> raise (ExpectingFun f)
    end

  | Apply (v,h,EPlus_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,EPlus_r (v,ctx,env)))

  | Apply (v2,h,EPlus_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        Inter (Apply (VNat MLR_Nat.(n1 + n2),h,ctx))
      | _ -> failwith "EPLUS"
    end

  | Apply (v,h,EMinus_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,EMinus_r (v,ctx,env)))

  | Apply (v2,h,EMinus_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        Inter (Apply (VNat MLR_Nat.(n1 - n2),h,ctx))
      | _ -> failwith "EPLUS"
    end

  | Apply (v,h,ELt_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,ELt_r (v,ctx,env)))

  | Apply (v2,h,ELt_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        Inter (Apply (VBool MLR_Nat.(n1 < n2),h,ctx))
      | _ -> failwith "ELT"
    end

  | Apply (v,h,EAnd_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,EAnd_r (v,ctx,env)))

  | Apply (v1,h,EAnd_r (v2,ctx,env)) ->
    begin match v1,v2 with
      | VBool b1,VBool b2 ->
        Inter (Apply (VBool (MLR_Bool.conj b1 b2),h,ctx))
      | _ -> failwith "EAnd_r"
    end
  | Apply (v,h,ENot ctx) ->
    begin match v with
      | VBool b -> Inter (Apply (VBool (MLR_Bool.not b),h,ctx))
      | _ -> failwith "ENot"
    end

  | Apply (v,h,ERef ctx) ->
    let (h,r) = alloc h v
    in Inter (Apply (VAddress r,h,ctx))

  | Apply (v,h,EBang ctx) ->
    begin match v with
      | VAddress r ->
        begin match H.get r h with
          | Some v -> Inter (Apply (v,h,ctx))
          | None -> raise AddressOutOfBounds
        end
      | VLam _ -> failwith "VLAM -> EBANG"
      | _ -> failwith "EBANG"
    end

  | Apply (v,h,EAttr_l (ctx,m,env)) ->
    Inter (Eval (m,env,h,EAttr_r (v,ctx,env)))

  | Apply (v2,h,EAttr_r (v1,ctx,env)) ->
    begin match v1 with
      | VAddress r ->
        let new_memory = H.update r v2 h
        in Inter (Apply (VUnit,new_memory,ctx))
      | _ -> failwith "EAttr_r"
    end

  | Apply (v,h,ELetIn (x,ctx,m,env)) ->
    Inter (Eval (m, H.update x v env,h,ctx))

  | Apply (v,h,EIfThenElse (ctx,t,f,env)) ->
    begin match v with
      | VBool b ->
        let m = if MLR_Bool.isTrue b then t else f
        in Inter (Eval (m,env,h,ctx))
      | _ -> failwith "EIfThenElse"
    end

(* A driver loop is a function iterating the transition function until a
   final state is reached. *)

let rec drive : state -> halting_state = function
  | Final h_state -> h_state
  | Inter conf -> drive (move conf)

let evaluate_ss env memory term : halting_state =
  drive (Inter (Eval (term,env,memory,EHole)))

let evaluate_toplevel evaluate (memory,env) (Let {vb_id;vb_exp}) =
  match evaluate env memory vb_exp with
  | Ok (h,v) ->
    (h, H.update vb_id v env)
  | Error e ->
    Printf.printf "error while evaluating %s." vb_id;
    raise e

let evaluate_program ?(env=H.empty) ?(evaluate = evaluate_ss) program =
  List.fold_left (evaluate_toplevel evaluate) (H.empty,env) program

(** Big step
    
    Small-step evaluation is the one used by default when evaluating a
    program, but one can use the following big step derivation instead.
    
    The derivation was exhibited by O.Danvy in [1].
    
    [1]: "On the equivalence between small-step and big-step abstract
         machines: a simple application of lightweight fusion"
         Olivier Danvy and Kevin Millikin
*)

let rec evaluate_bs conf = match conf with

  (* Evaluation part *)

  | Eval (Var x,env,h,ctx) ->
    begin match H.get x env with
      | Some v -> evaluate_bs (Apply (v,h,ctx))
      | None -> fail (UnboundVariable x)
    end

  | Eval (Lam (x,t),env,h,ctx) ->
    evaluate_bs (Apply (VLam (x,Closure (t,env)),h,ctx))

  | Eval (Nat n,env,h,ctx) ->
    evaluate_bs (Apply (VNat n,h,ctx))

  | Eval (Bool b,env,h,ctx) ->
    evaluate_bs (Apply (VBool b,h,ctx))

  | Eval (Address p,env,h,ctx) ->
    begin match H.get p h with
      | Some v -> evaluate_bs (Apply (v,h,ctx))
      | None -> fail AddressOutOfBounds
    end

  | Eval (Unit,env,h,ctx) ->
    evaluate_bs (Apply (VUnit,h,ctx))

  | Eval (App (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,EApp_l (ctx,m2,env)))

  | Eval (Plus (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,EPlus_l (ctx,m2,env)))

  | Eval (Minus (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,EMinus_l (ctx,m2,env)))

  | Eval (Lt (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,ELt_l (ctx,m2,env)))

  | Eval (And (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,EAnd_l (ctx,m2,env)))

  | Eval (Not m,env,h,ctx) ->
    evaluate_bs (Eval (m,env,h,ENot ctx))

  | Eval (Ref m,env,h,ctx) ->
    evaluate_bs (Eval (m,env,h,ERef ctx))

  | Eval (Bang m,env,h,ctx) ->
    evaluate_bs (Eval (m,env,h,EBang ctx))

  | Eval (Attr (m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,EAttr_l (ctx,m2,env)))

  | Eval (LetIn (x,m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (m1,env,h,ELetIn (x,ctx,m2,env)))

  | Eval (IfThenElse (b,m1,m2),env,h,ctx) ->
    evaluate_bs (Eval (b,env,h,EIfThenElse (ctx,m1,m2,env)))

  (* Application part *)

  | Apply (v,h,EHole) ->
    return (h,v)

  | Apply (v,h,EApp_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,EApp_r (v,ctx,env)))

  | Apply (v,h,EApp_r (f,ctx,env)) ->
    begin match f with
      | VLam (x,Closure (t,env)) ->
        evaluate_bs (Eval (t,H.update x v env,h,ctx))
      | _ -> raise (ExpectingFun f)
    end

  | Apply (v,h,EPlus_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,EPlus_r (v,ctx,env)))

  | Apply (v2,h,EPlus_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        evaluate_bs (Apply (VNat MLR_Nat.(n1 + n2),h,ctx))
      | _ -> failwith "EPLUS"
    end

  | Apply (v,h,EMinus_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,EMinus_r (v,ctx,env)))

  | Apply (v2,h,EMinus_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        evaluate_bs (Apply (VNat MLR_Nat.(n1 - n2),h,ctx))
      | _ -> failwith "EPLUS"
    end

  | Apply (v,h,ELt_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,ELt_r (v,ctx,env)))

  | Apply (v2,h,ELt_r (v1,ctx,env)) ->
    begin match (v1,v2) with
      | VNat n1,VNat n2 ->
        evaluate_bs (Apply (VBool MLR_Nat.(n1 < n2),h,ctx))
      | _ -> failwith "ELT"
    end

  | Apply (v,h,EAnd_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,EAnd_r (v,ctx,env)))

  | Apply (v1,h,EAnd_r (v2,ctx,env)) ->
    begin match v1,v2 with
      | VBool b1,VBool b2 ->
        evaluate_bs (Apply (VBool (MLR_Bool.conj b1 b2),h,ctx))
      | _ -> failwith "EAnd_r"
    end
  | Apply (v,h,ENot ctx) ->
    begin match v with
      | VBool b -> evaluate_bs (Apply (VBool (MLR_Bool.not b),h,ctx))
      | _ -> failwith "ENot"
    end

  | Apply (v,h,ERef ctx) ->
    let (h,r) = alloc h v
    in evaluate_bs (Apply (VAddress r,h,ctx))

  | Apply (v,h,EBang ctx) ->
    begin match v with
      | VAddress r ->
        begin match H.get r h with
          | Some v -> evaluate_bs (Apply (v,h,ctx))
          | None -> raise AddressOutOfBounds
        end
      | VLam _ -> failwith "VLAM → EBANG"
      | _ -> failwith "EBANG"
    end

  | Apply (v,h,EAttr_l (ctx,m,env)) ->
    evaluate_bs (Eval (m,env,h,EAttr_r (v,ctx,env)))

  | Apply (v2,h,EAttr_r (v1,ctx,env)) ->
    begin match v1 with
      | VAddress r ->
        let new_memory = H.update r v2 h
        in evaluate_bs (Apply (VUnit,new_memory,ctx))
      | _ -> failwith "EAttr_r"
    end

  | Apply (v,h,ELetIn (x,ctx,m,env)) ->
    evaluate_bs (Eval (m, H.update x v env,h,ctx))

  | Apply (v,h,EIfThenElse (ctx,t,f,env)) ->
    begin match v with
      | VBool b ->
        let m = if MLR_Bool.isTrue b then t else f
        in evaluate_bs (Eval (m,env,h,ctx))
      | _ -> failwith "EIfThenElse"
    end

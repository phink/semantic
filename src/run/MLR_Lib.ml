open MLR_AST

module MLR_Nat = struct

  let rec ( + ) a b = match a with
    | Zero   -> b
    | Succ a -> Succ (a + b)

  let rec ( - ) a b = match (a,b) with
    | (Zero,_) | (_,Zero) -> a
    | (Succ a, Succ b) -> a - b

  let add = ( + )

  let sub = ( - )

  let rec ( < ) a b = match (a,b) with
    | (Zero, Succ _) -> True
    | (Succ a, Succ b) -> a < b
    | _ -> False

  let lt = ( < )

end

module MLR_Bool = struct

  let conj a b = match a with
    | True  -> b
    | False -> False

  let isTrue x = x = True

  let not = function
    | True  -> False
    | False -> True
end

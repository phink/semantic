{

open Lexing
open MLR_Parser
    
let next_line_and f lexbuf  =
  Lexing.new_line lexbuf;
  f lexbuf

let kwds =
  [ "and"   , AND
  ; "else"  , ELSE
  ; "false" , FALSE
  ; "fun"   , FUN
  ; "if"    , IF
  ; "in"    , IN
  ; "let"   , LET
  ; "not"   , NOT
  ; "then"  , THEN
  ; "true"  , TRUE
  ; "ref"   , REF
  ]
  
let kwd_tbl =
  let size = 32 in
  let htbl = Hashtbl.create size in
  List.iter (fun (k,v) -> Hashtbl.add htbl k v) kwds;
  htbl
  
let find_kwd s = Hashtbl.find kwd_tbl s   
}

let dot = '.'

let newline = ('\013'* '\010')

let blank = [' ' '\009' '\012']

let symbol = ['+' '-' '*' '/' '<' '=' '>']

let digit = ['0'-'9']+

let lowercase = ['a'-'z']

let uppercase = ['A'-'Z']

let alpha = lowercase | uppercase

let alphanum = alpha | digit | '_'

let lident = lowercase alphanum* ['\'']*

let int_literal = digit+

rule token = parse
  | newline
      { next_line_and token lexbuf }
  | blank+
      { token lexbuf }
  | "--"
      { comment_inline lexbuf }
  | "(*"
      { comment_block 0 lexbuf
      ; token lexbuf
      }
  | "("
      { LPAREN }
  | ")"
      { RPAREN }
  | lident as id
      { try  find_kwd id
	with Not_found -> LIDENT id
      }
  | int_literal as n
      { NAT (int_of_string n) }
  | "="
      { EQUAL }
  | "→" | "->"
      { MINUSGREATER }
  | ":="
      { COLONEQUAL }
  | ";"
      { SEMI }
  | "!"
      { BANG }
  | "<"
      { LESS }
  | "+"
      { PLUS }
  | "-"
      { MINUS }
  | eof
      { EOF }

and comment_block level = parse
  | "*)" {if level = 0 then ()
	  else comment_block (pred level) lexbuf}
  | "(*" {comment_block (succ level) lexbuf}
  |  _   {comment_block level lexbuf}

and comment_inline = parse
  | (newline | eof)  { next_line_and token lexbuf }
  | _                { comment_inline lexbuf }

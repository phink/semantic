(* An identifiant is a string *)

type id = string

type reference = int

(* The type of natural numbers and booleans *)

type nat = Zero | Succ of nat

type boolean = True | False

(* Expressions *)

type t = decl list

and decl =
  | Let of binding

and binding =
  { vb_id : string
  ; vb_exp : expr
  }

and expr =
  (* 𝑥 *)
  | Var of id
  (* λ𝑥.M *)
  | Lam of id * expr
  (* 𝑛 *)
  | Nat of nat
  (* 𝑏 *)
  | Bool of boolean
  (* 𝑝 *)
  | Address of reference
  (* () *)
  | Unit
  (* @(M,M) *)
  | App of expr * expr
  (* M + M *)
  | Plus of expr * expr
  (* M - M *)
  | Minus of expr * expr
  (* M < M *)
  | Lt of expr * expr
  (* M ∧ M *)
  | And of expr * expr
  (* ¬ M *)
  | Not of expr
  (* ref M *)
  | Ref of expr
  (* !M *)
  | Bang of expr
  (* M ≔ M *)
  | Attr of expr * expr
  (* let x = M in M *)
  | LetIn of id * expr * expr
  (* if M then M else M *)
  | IfThenElse of expr * expr * expr

                      
(* The infix symbol ( $ ) has left associativity so [a $ b $ c ≡ (a $ b) $ c] *)

let ( $ ) a b = App (a,b)

let ( +++ ) m n = Plus (m,n)
let ( --- ) m n = Minus (m,n)
let lt m n = Lt (m,n)
let ( & ) m n = And (m,n) 
let ( !- ) n = Not n
let mk_ref r = Ref r
let bang a = Bang a
let ( =:= ) r e = Attr (r,e)
let if_then_else b t f = IfThenElse (b,t,f)

let rec fold_app p = function
  | [] -> Var p
  | x::xs -> App (fold_app p xs, Var x)

let rec collapse_lam f = function
  | Lam (x,t) -> Lam (x, collapse_lam f t)
  | m -> f m

let unfold_lam t xs =
  let rec aux = function
    | [] -> t
    | x::xs -> Lam (x, aux xs)
  in aux xs

let rec remove x xs =
  let rec aux = function
    | [] -> []
    | y :: xs when y = x -> aux xs
    | y :: xs -> y :: aux xs
  in aux xs

let rec ftv t =
  let rec ftv = function
    | Var y -> [y]
    | Lam (x,t) -> remove x (ftv t)
    | Nat _ | Bool _ | Address _ | Unit -> []
    | App (t1,t2) | Plus (t1,t2) | Minus (t1,t2) | Lt (t1,t2)
    | Attr (t1,t2) | And (t1,t2) -> ftv t1 @ ftv t2
    | Not t | Ref t | Bang t -> ftv t
    | IfThenElse (t1,t2,t3) -> ftv t1 @ ftv t2 @ ftv t3
    | LetIn (x,m,n) -> ftv m @ remove x (ftv n)
  in ftv t

%{

open MLR_AST

let mk_apply s xs =
  List.fold_left (fun acc x -> App (acc, x)) s xs

let unfold_nat n =
  if n < 0 then failwith "negative number";
  let rec aux acc n =
    if n = 0 then acc
    else aux (Succ acc) (pred n)
  in aux Zero n

let unfold_fun ps e =
  let rec aux acc = function
    | [] -> acc
    | p::ps -> aux (Lam (p,acc)) ps
  in aux e (List.rev ps)
  
	 
%}

(* Tokens. *)

%token AND
%token BANG
%token COLONEQUAL
%token ELSE
%token EOF
%token EQUAL
%token FALSE
%token FUN
%token IF
%token IN
%token MINUS
%token MINUSGREATER
%token <int> NAT
%token NOT
%token LESS
%token LET
%token LPAREN
%token <string> LIDENT
%token PLUS
%token REF
%token RPAREN
%token SEMI
%token THEN
%token TRUE
(* %token <string> UIDENT *)

(* Precedences and associativities. *)

%nonassoc IN
%nonassoc SEMI
%nonassoc ELSE
%right    COLONEQUAL
%right    AND
%right    MINUSGREATER
%left     LESS
%left     PLUS MINUS
(* %nonassoc BANG NOT REF *)

(* Entrance. *)
%start<MLR_AST.t> program
%%

program: structure EOF { $1 }

structure:
/* empty */                 { [] }
| structure_item structure  { $1 :: $2 }

structure_item:
| let_binding        { Let $1 }

(*********************************************)
(*************** EXPRESSION ******************)
(*********************************************)

let_binding:
| LET lident ps=list(lident) EQUAL e=expr
   { let new_exp = unfold_fun ps e
     in {vb_id = $2; vb_exp = new_exp}
   }

expr:
| simple_expr
  { $1 }
| simple_expr nonempty_list(simple_expr)
  { mk_apply $1 $2 }
| FUN lident MINUSGREATER expr
  { Lam ($2,$4) }
| expr PLUS expr
  { Plus ($1,$3) }
| expr MINUS expr
  { Minus ($1,$3) }
| IF expr THEN expr ELSE expr
  { IfThenElse ($2,$4,$6) }
| expr LESS expr
  { Lt ($1,$3) }
| expr AND expr
  { And ($1,$3) }
| LET lident list(lident) EQUAL expr IN expr
  { let new_exp = unfold_fun $3 $5
    in LetIn ($2,new_exp,$7)
  }
| expr COLONEQUAL expr
  { Attr ($1,$3) }
| expr SEMI expr
  { App (Lam ("()", $3), $1) }

simple_expr:
| LPAREN RPAREN
  { Unit }
| LPAREN expr RPAREN
  { $2 }
| lident
  { Var $1 }
| BANG simple_expr
  { Bang $2 }
| REF simple_expr
  { Ref $2 }
| NOT simple_expr
  { Not $2 }
| TRUE
  { Bool True }
| FALSE
  { Bool False }
| NAT
  { Nat (unfold_nat $1)
  }

(* shortcuts *)
		   
lident:
 | LIDENT { $1 }

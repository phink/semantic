open MLR_AST
open MLR_PPrint
open MLR_Run
open MLR_Types
open MLR_Interface

module Swallow = struct
  
  let index = ref 0
  let initial = Bytes.create 256
  let buffer = ref initial
      
  let reset () =
    buffer := initial;
    index := 0
      
  let store c =
    if !index >= Bytes.length !buffer then
      begin
        let new_buffer = Bytes.create (Bytes.length !buffer * 2)
        in Bytes.blit !buffer 0 new_buffer 0 (Bytes.length !buffer);
        buffer := new_buffer
      end;
    Bytes.unsafe_set !buffer !index c;
    incr index
      
  let get () =
    let s = Bytes.sub_string !buffer 0 !index
    in reset ();
    s
end    

let read () =
  let open Swallow in
  let rec read () =    
    match input_char stdin with
    | exception End_of_file -> get ()
    | c -> store c; read ()
  in parse_string (read ())

let print_mode mode =
  Printf.printf "\n------ %s ------\n" mode
    
let print_expression expr =
  Printf.printf "-- Resulting expression\n";
  print_string (print print_lets expr)
    
let activate (module T : TRANS) ?(env : Gamma.t option = None) program
  : (module ACTIVE) =
  (module struct
    let translation =
      T.transform program
    let ty_env = match env with
      | None -> Gamma.to_list (typecheck ~ty_env:T.initial_tyenv translation)
      | Some env -> Gamma.to_list env
    let (memory,env) =
      evaluate_program ~env:T.initial_env translation
    let display () =
      print_mode T.name;
      print_expression translation;
      print (print_infos memory env ty_env) translation
  end)

let print_full_trans () =

  let program = read () in
  let ty_env = typecheck program in
  
  (* identity *)
  let id =
    activate (module MLR_Identity) ~env:(Some ty_env) program
  in print_string (display id ());

    (* Continuation passing style *)
    let cps =
      activate (module MLR_CPS) ~env:(Some ty_env) (translation id)
    in print_string (display cps ());

    (* Value named form *)
    let vnf =
      activate (module MLR_VNF) ~env:(Some ty_env) (translation cps)
    in print_string (display vnf ());

    (* Closure conversion *)
    let ccc =
      activate (module MLR_CCC) ~env:(Some ty_env) (translation vnf)
    in print_string(display ccc ());

    (* Hoisting *)
    let hoist =
      activate (module MLR_Hoisting) ~env:(Some ty_env) (translation ccc)
    in print_string (display hoist ())
                      
let print_usage () =
  let open Printf in
  printf "Usage :\n";
  printf "\t ./MLR_Toplevel < <source_file> :";
  printf "print only the result of the translated program.\n";
  printf "\t ./MLR_Toplevel -verbose < <source_file> :";
  printf "print all the sequence of transformations.\n"

let () = print_full_trans ()

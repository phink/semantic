ML-Ref
=====

### Prelude

**ML-Ref** is a small functional language (ML-like) with side effects
(mutable references). It has:

- Type inference and polymorphism (with value restriction).  
  (path `/src/typing`)
- A small-step and a big-step interpreter with explicit contexts.  
  (path `/src/run`)
- A set of program transformations (Continuation by passing style,
  Value named form, Closure conversion and Hoisting).  
  (path `/src/trans`)
- A testsuite that works with my `Teddy` bear.  
  (path `/testsuite`)

Some parts of the code are using papers (comments mention it when it is the case).

### Dependances

This project requires `oasis` and `pprint` to be installed.
That can easily be done via the fabulous packet manager opam.

```
opam install pprint
opam install oasis
```

### Building ML_Ref

First of all you need to clone the repository.  
Then you have to setup the oasis environment.

```
oasis setup
```

So you can build the project.

```
make
```

### Usage

After building the project, there is an executable named `MLR.native` created
at the root of the project. Yo can use it as follows:

```
./MLR.native < playground/example.mlref
```

This will display all the step of the transformations, and use
the small-step interpreter to evaluate them.

**Here is the BNF for the language**

```
inline-comment :       à la haskell
	-- <your comment>

comment:               à la ocaml
	(* <your comment> *)

id  = [a-z]+
nat = [0,1,2,..,9]+

program = decl list

decl ::= let <id> = M

B,M,N ::=
	| fun id → M | M N
	| nat | () | true | false
	| ref M | M := N | !M
	| M + N | M < N | M - N
	| if B then M else N | M and N
	| let id = M in N
```

### To enable the testsuite

The testsuite uses my `Teddy` bear.
Its content is in `/teddy`, at the root of the repository (if you are curious).

To enable the test runner you need to tell it to oasis:

```
ocaml setup.ml -configure --enable-tests
```

So you can run the testsuite:

```
make test

./Testsuite.native
```

What we want to test is

```
if Γ ⊢ t : τ ↓ v then Γ ⊢ HOISTING ∘ CCC ∘ VNF ∘ CPS (t) : τ ↓ v
```

Hence, the test runner proceeds as follows:

1. It checks that the input program has the correct output when evaluating it
   via the small-step interpreter w.r.t an expected file.
2. Then it checks that the input program fully transformed has also the correct
   expectation.

###### Adding a test file

If you want to add a test, you need to

1. create a file named `<your filename>.input.mlref` which contain
   your source program,
2. create a file named `<your filename>.expected.mlref` which contain
   the expected output.

If the test fails, this will generate a file `<your filename>.output.mlref`
containing the real output.

### Contact

If there is a problem, do not hesitate to contact me !!!

```paul.laforgue123@gmail.com```
